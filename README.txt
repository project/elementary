CONTENTS
--------

  * Introduction
  * Installation
  * Technical information
  * Author


INTRODUCTION
------------

Elementary is a minimalistic Drupal theme primarily aimed at blogs.

The main aim of Elementary is to be as simple as possible and really
focus on the text content of a blog above all else.

The theme comes with two settings to control the font-style of the site,
you can choose to use a Serif or Sans-serif font via the settings page.


INSTALLATION
------------

1. Place elementary into the sites/all/themes folder
2. Enable the theme at admin/appearance
3. Change the default settings at admin/appearance/settings/elementary


TECHNICAL INFORMATION
---------------------

The theme contains a number of template overrides which aim to remove some of
the markup provided by default.

The theme contains a couple of custom settings:

1. CSS Overrides - gives the option to remove ALL other CSS provided by
   core & contrib modules for logged out users.
2. Fonts - gives the option to switch between a serif & sans-serif font.


AUTHOR
------

Elementary is designed & developed by Thomas Davis
http://twitter.com/TEDavis
http://designedbythomas.co.uk
